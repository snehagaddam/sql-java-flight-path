
import java.sql.Connection;
import java.sql.DriverManager; 
import java.sql.SQLException; 
import java.sql.Statement; 
import java.sql.PreparedStatement; 
import java.sql.ResultSet; 

public class Path {
	
	public static void main (String[] args) {
		Connection conn = null; 

		try {
			Class.forName("org.sqlite.JDBC"); 
			conn = DriverManager.getConnection("jdbc:sqlite:path.db"); 
			System.out.println("opened database successfully.");

			Statement stmt = conn.createStatement();
			
			stmt.executeUpdate("DROP TABLE IF EXISTS Connected; "); 
			stmt.executeUpdate("CREATE TABLE Connected(Airline, Origin, Destination, Stops);"); 

			stmt.executeUpdate("DROP TABLE IF EXISTS Current; "); 
			stmt.executeUpdate("CREATE TABLE Current(Airline, Origin, Destination, Stops);");
			stmt.executeUpdate("INSERT INTO Current SELECT *, 0 FROM Flight;");
		
			stmt.executeUpdate("DROP TABLE IF EXISTS Prev;"); 
			stmt.executeUpdate("CREATE TABLE Prev(Airline, Origin, Destination, Stops);");

			stmt.executeUpdate("DROP TABLE IF EXISTS Temp;"); 
			stmt.executeUpdate("CREATE TABLE Temp(Airline, Origin, Destination);");
			stmt.executeUpdate("INSERT INTO Temp(Airline, Origin, Destination) SELECT Airline, Origin, Destination FROM Flight;"); 
			
			
			ResultSet rset = stmt.executeQuery("SELECT COUNT(*) FROM Temp;"); 
			int numEntries = rset.getInt(1);  
			int count = 1;

			while( numEntries > 0) {

				stmt.executeUpdate("DELETE FROM Prev;");
				stmt.executeUpdate("INSERT INTO Prev SELECT * FROM Current;"); 
				stmt.executeUpdate(" INSERT INTO Current "
					+ " SELECT t.Airline, t.Origin, f.Destination, " + count  
					+ " FROM Flight f, Temp t "
					+ " WHERE t.Destination = f.Origin AND f.Airline = t.Airline and f.Destination != t.origin;"); 
				
				stmt.executeUpdate("DELETE FROM Temp;"); 
				stmt.executeUpdate("INSERT INTO TEMP SELECT Airline, Origin, Destination FROM Current EXCEPT "
					+ "SELECT Airline, Origin, Destination FROM Prev;"); 
				
				rset = stmt.executeQuery("SELECT COUNT(*) from Temp;");
				numEntries = rset.getInt(1); 
				count++;
				 
			} 
			
			stmt.executeUpdate("DROP TABLE IF EXISTS Prev; "); 
			stmt.executeUpdate("DROP TABLE IF EXISTS Temp; "); 

			stmt.executeUpdate("INSERT INTO Connected SELECT Airline, Origin, Destination, MIN(Stops) FROM Current GROUP BY Airline, Origin, Destination;"); 
			
			stmt.executeUpdate("DROP TABLE IF EXISTS Current;");
		
			rset.close(); 	
			stmt.close(); 
		}

		catch (Exception e) {
			throw new RuntimeException("There was a runtime problem!", e); 
		}

		finally {
			try {
				if (conn != null) {
					conn.close(); 
				}
			}
			catch (SQLException e) {
				throw new RuntimeException("Cannot close the connection", e); 
			}
		}
	}
}
			

