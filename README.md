
Given the path database, which contains the Flight table, the number of connections required to travel from any given origin to destination are determined. Results are outputed in Connected table.   

Example of Flight table:  
-----------------------

| airline | origin | destination |
| --- | --- | --- |
| Delta | Barcelona, Spain | Dublin, Ireland |


Example of Connected table:  
---------------------------

| airline | origin | destination | stops |
| --- | --- | --- | --- |
| Delta | Paris, France | Athens, Greece | 2 |
